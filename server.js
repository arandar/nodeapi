const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const db = require('./config/db');

const app = express();

let users = [{ name: "Bob", age: 34, }, { name: "Alice", age: 21 }, { name: "Ross", age: 22 }];
let user = { name: "Tom", age: 23, _id: 1 };

MongoClient.connect(db.url, (error, client) => {
	const database = client.db('usersdb');
	const collection = database.collection('users');
	//запись одного объекта
	// collection.insertOne(user, (error, result) => {
	// 	if(error) throw error;
	// 	console.log(result.ops);
	// 	client.close();
	// });

	// запись множества объектов
	// collection.insertMany(users, (error, results) => {
	// 	console.log(results);
	// 	client.close();
	// });

	// вывод данных в виде массива
	if (error) throw error;
	collection.find().toArray((error, results) => {
		console.log(results);
		client.close();
	});

	// удаление всех по критерию
	// collection.deleteMany({ name: "Tom" }, (error, result) => {
	// 	console.log(result);
	// 	client.close();
	// });

	// // удаление одного объекта по критерию
	// collection.deleteOne({ name: "Bob" }, (error, result) => {
	// 	console.log(result);
	// 	client.close();
	// });

	// удаление одного по критерию, но возвращает удаленный объект
	// collection.findOneAndDelete({ age: 21 }, (error, result) => {
	// 	console.log(result);
	// 	client.close();
	// });

	// удаление всей коллекции
	// collection.drop((error, result) => {
	// 	console.log(result);
	// 	client.close();
	// });

	// обновление одного элемента по критерию и возврат объекта
	// collection.findOneAndUpdate(
	// 	{ age: 25 },
	// 	{ $set: { age: 21 } }, // на что меняем
	// 	{ returnOriginal: false }, // если хотим, чтобы отображался измененный объект, а не старая версия
	// 	(error, result) => {
	// 		console.log(result);
	// 		client.close();
	// 	}
	// );

	// обновляет все объекты по критерию
	// collection.updateMany(
	// 	{ name: "Bob" },
	// 	{ $set: { name: "Sam" } },
	// 	(error, result) => {
	// 		console.log(result);
	// 		client.close();
	// 	}
	// );

	// обновляет один объект по критерию
	// collection.updateOne(
	// 	{ name: "Sam" },
	// 	{ $set: { name: "Bob", age: 11 } },
	// 	(error, result) => {
	// 		console.log(result);
	// 		client.close();
	// 	}
	// );
});

// app.listen(3000);
